// ==UserScript==
// @name         OLX GPU Helper
// @version      1.0.12
// @author       Uran
// @match        https://www.olx.ua/elektronika/*videokarty*
// @grant        GM_xmlhttpRequest
// @grant        GM_setValue
// @grant        GM_getValue
// ==/UserScript==

/* jshint ignore: start */

let futuremark;

class CGPU {
    constructor(title) {
        this.vendors = [
            {name: "Nvidia", regular: /(GTX?S?) ?(\d{3,4}) ?(Ti|SE)?/i},
            {name: "Radeon", regular: /(HD) ?(\d{3,4})/i},
            {name: "AMD", regular: /(R\d|RX).+?(\d{3}X?)/i}
        ];

        this.parse(title);
    }

    vendorIndex(title) {
        let result = -1;
        for (var i = 0; i < this.vendors.length; i++) {
            if (this.vendors[i]['regular'].test(title)) {
                result = i;
                break;
            }
        }
        return result;
    }

    parse(title) {
        let vendor = this.vendorIndex(title);

        if (vendor < 0) return;

        let card = this.vendors[vendor]['regular'].exec(title);

        if (vendor == 0)
            this.Suffix = card.length > 2 ? card[3] : "";

        this.Prefix = card[1].toUpperCase();
        this.Model = card[2];
    }

    async score() {
        debugger;
        if (futuremark == null) {
            console.warn("Futuremark response is empty :(");
            return;
        }

        let futuremark_score = new RegExp(this.toString() + '<.{1,1000}\'bar-score\'>\\n\\s{1,15}(\\d+)', "s");

        if (!this.valid()) return 0;

        if (futuremark_score == null)
            console.warn("Futuremark score is empty!");

        let score = futuremark_score.exec(futuremark);

        return score != null ? score[1] : 0;
    }

    valid() {
        return this.Model != null;
    }

    toString() {
        let Prefix = this.Prefix != null ? this.Prefix + " " : "";
        let Model = this.Model != null ? this.Model : "UNKNOWN";
        let Suffix = this.Suffix != null ? " " + this.Suffix : "";
        return Prefix + Model + Suffix;
    }
}

async function InjectSubInformation(element) {
    let title = element.querySelector('.lheight22 strong').textContent,
        injectPoint = element.querySelector('.price').parentElement,
        price = element.querySelector('.price strong').textContent.replace(/\D/g, ''),
        GPU = new CGPU(title),
        score = await GPU.score(),
        rating = score / price;

    /*
    if (!GPU.valid()) return;
    if (score == 0) return;
    */

    if ((rating < 3) | (!GPU.valid()) | (score == 0)) {
        element.style.display = 'none';
        return;
    }

    injectPoint.insertAdjacentHTML('beforeEnd', `<p class="price" style="zoom: .5;">${GPU.toString()} (${score})</p>`);
    injectPoint.insertAdjacentHTML('beforeEnd', `<p class="price" style="font-size:${rating * 40}%">${rating.toFixed(1)}</p>`);
}

async function loadFuturemark() {
    GM_xmlhttpRequest({
        method: 'get',
        url: 'https://benchmarks.ul.com/compare/best-gpus',
        onload: (response) => {
            futuremark = response.responseText;
            GM_setValue("GPU", futuremark);
        },
    });

    console.warn("Futuremark site loaded.");
}

function track(name, callback) {
    document.querySelectorAll(name).forEach(function(element) {
        callback(element);
    });

    var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            mutation.addedNodes.forEach(function(node) {
                if (node.querySelectorAll)
                    node.querySelectorAll(name).forEach(function(selector) {
                        callback(selector);
                    })})})});

    observer.observe(document.body, {childList: true, subtree: true});
    return observer;
}

async function main() {
    futuremark = await GM_getValue("GPU", null);

    if (futuremark == null)
        await loadFuturemark();

    track('.offer', InjectSubInformation);
}

main();